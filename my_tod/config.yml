---
user:
  name: Mathias De Roover
  job: Embedded Engineer
  header:
    expertise:
      - Linux
      - C
      - C++
      - Python
    languages:
      - Dutch
      - English
    location: Beringen
    mobility:
      - Limburg
      - Flemish Brabant
      - Antwerp
  projects:
    - name: Embedded Linux Traineeship
      start: September 2019
      end: November 2019
      company: Nalys
      location: Brussels
      context: "\\textbf{Nalys} is a consulting group dedicated to high-technology projects. In order to create better than regular junior engineers, the Nalys Institute of Technology (NIT) organizes the \\textbf{Traineeship}:  a hands-on industrial grade training program. During the first 8 weeks of the program, the engineers are taught in hard and soft skills. This training is organized in theory and lab sessions, with an emphasis on practical workshops. It teaches the engineers to work under pressure and at a high pace. After the first 8 weeks of trainings, a 4-week \\textbf{Graduation Project} is organized in which a real-life problem is solved together with a client. After the Traineeship, the engineers are coached by experienced Nalys engineers."
      context_short: Graduate program as preparation in embedded Linux.
      achievements:
        - "\\textbf{Embedded C:} Cross-compiling, Makefile, Bare metal programming \\& RTOS, I/O peripherals, interrupts, ARM Cortex-M4 development board, device drivers, labs on distributed sensor network"
        - "\\textbf{Networking:} Networking theory, TCP/IP, ssh, server/client, protocol stacks, labs on edge nano firewall"
        - "\\textbf{Automotive:} CAN, RTOS, ARM Cortex-M4 development board"
        - "\\textbf{Embedded Linux:} Buildroot, Yocto, U-Boot, Linux Kernel, System Programming, ARM Cortex-A7 development board, Qemu emulation environment, labs on IoT gateway"
        - "\\textbf{Professional focus:} Code quality, Review process, Professional documentation, Communication, Eye for detail"
        - "\\textbf{Critical Thinking:} Problem solving, Root cause analysis, Autonomy, Efficient working"
        - "\\textbf{Development lifecycle:} Scrum, CI/CD, Unit testing, Feature testing, Smoke testing, Integration testing"
      achievements_short:
        - "Linux basics, Networking and C"
        - "Embedded programming"
    - name: Nalys Traineeship Graduation Project
      start: November 2019
      end: December 2019
      company: Melexis
      location: Tessenderlo
      context: "After the traineeship a \\textbf{graduate project} was organized in which the trainees got hands on experience working on a real project under the supervision of an experienced Nalys engineer. For my graduate project I was hired by \\textbf{Melexis} to improve the \\textbf{CI (Continuous Integration)} of their video transfer project. The context of this project was to transfer frames captured by a TOF sensor from an embedded target to a remote target. This transfer happens using UDP/IP and the frames are transferred in an uncompressed manner. The \\textbf{Gstreamer} framework was used to set up the communication between the server and client. The CI was improved by adding static code analysis using \\textbf{Coverity} and multiple \\textbf{integration tests}. These integration tests assured that the server and client could correctly connect, disconnect, transfer frames, and that the client could successfully get and set server parameters. This increased the efficiency of this project's development team."
      context_short: Improve CI of ToF sensor video transfer project
      achievements:
        - "\\textbf{Development lifecycle:} CI, Integration testing, Coverity"
        - "\\textbf{Networking:} UDP/IP"
        - "\\textbf{Multimedia processing:} Gstreamer"
      achievements_short:
        - "Integration testing and static code analysis"
        - "Multimedia processing with Gstreamer"
    - name: Melexis ALM tool support
      start: December 2019
      end: January 2020
      company: Melexis
      location: Tessenderlo
      context: "During the month of December I was employed by Melexis to offer support for their Application Lifecycle Management (ALM) tool, namely Polarion. I was to solve all issues related to this ALM software and to be the technical expertise for the team which was developing the workflow to be supported by the tool. A series of scripts written in Apache Velocity Template Language were also developed for reporting purposes."
      context_short: Providing technical expertise for Polarion and scripting in VTL
      achievements:
        - "\\textbf{Application Lifecycle Management}: Polarion"
        - "\\textbf{Scripting}: Apache Velocity Template Language"
      achievements_short:
        - "Application Lifecycle Management Support"
    - name: Thesis
      start: September 2018
      end: June 2019
      company: VUB
      location: Brussels
      context: "To achieve my Masters degree I had to successfully complete a thesis. This thesis was about algorithmic optimization of JPEG Pleno for light-field displays. JPEG Pleno is a standardization effort based on the plenoptic function. It aims to provide compression and interoperability for modalities like holograms, point-clouds, and light-fields.\\\\ My job existed out of optimizing the compression scheme used by JPEG Pleno for light-fields. This was done partly by offloading work to the GPU using OpenCL and partly by adapting algorithms to make this parallelization possible, all the while increasing the overall speed and keeping memory consumption minimal.\\\\The developed algorithms themselves are roughly based on the 4-dimensional DCT, optimal rate/distortion behaviour through the use of Lagrange multipliers, hexadeca-tree encoding, and context adaptive binary arithmetic coders."
      context_short: Algorithmic optimization of the JPEG Pleno codec for light-field displays.
      achievements:
        - "\\textbf{Algorithm:} Light-field compression using arithmetic coders, tree encoding, and Lagrange multipliers"
        - "\\textbf{Result:} Speed of the 4D DCT transform improved with more than x100. Overall compression speed reduced from +40s to less than 10. Quality improved with over 10 dB."
        - "\\textbf{Technologies:} C++, OpenCL, VirtualBox, Linux"
        - "\\textbf{Skills:} General purpose computing on GPU, algorithm development"
      achievements_short:
        - "Light-field compression"
        - "General purpose computing on GPU: OpenCL"
    - name: Internship
      start: July 2018
      end: September 2018
      company: NXP
      location: Leuven
      context: "NXP SOFTWARE is a branch within NXP that focusses mostly on audio solutions. These audio solutions are specifically aimed towards echo cancellation and noise suppression for both the mobile phone sector and since recently the car sector. Their solutions are branded as Voice experience and are all build on top of their custom Voice Experience Engine.\\\\The goal of this internship was to continue work on an automated tuning tool for the Voice Experience Engine and by extension most of their audio solutions.\\\\The tuning itself was based on a genetic algorithm with some speech recognition based metrics as a cost function. The automated tuning took days to complete which is not usable in a practical context for a mobile phone manufacturer. I helped move this automated tuning to the cloud (AWS), distributing this tuning across a cluster using ECS by which this tuning time was reduced to hours instead of days.\\\\Besides this a backend webserver based on Django was developed as well. This webserver used a basic frontend based on Django's own template engine. This backend served as a central point from which to launch tuning jobs and of which to request meta data of these tuning jobs. The server itself was build to run on AWS beanstalk.\\\\Besides all this, Django Rest Framework was used as well to create a RESTful API through which a client-side application could launch tuning jobs.\\\\Overall, a tool was delivered that could launch and interface with tuning jobs through both a webpage and a client-side application, and that could run these tuning jobs spread across a cluster reducing the needed calculation time. Within the context of this internship I also had my first contact with the Agile approach. NXP teams have a scrum master, hold daily stand-up meetings, and organize sprints."
      context_short: Improving an automated tuning tool for NXPs' Voice Experience Engine
      achievements:
        - "\\textbf{Algorithm:} genetic algorithm to find best parameters"
        - "\\textbf{Result:} Improved auto tuning tool, overall computation time reduced from days to hours. Webserver with RESTful API implemented to control tuning-jobs."
        - "\\textbf{Technologies:} Django, Django REST Framework, Amazon AWS, ECS, Elastic Beanstalk, boto3, SVN."
        - "\\textbf{Skills:} Use of Agile Approach Scrum, Teamwork, cloud and web development."
      achievements_short:
        - "Improved automated tuning tool"
        - "Webserver developed for use with Amazon AWS"
    - name: Image and Video Technologies
      start: October 2018
      end: December 2018
      company: VUB
      location: Brussels
      context: "In the context of the IVT course there was a project for image compression. The project entailed creating a compression (and decompression) scheme for standard 2D images that is roughly based on JPEG. Multiple compression techniques were used in this project among which: DCT transforms, Quantization tables, Runlength-coding, Delta-coding, Arithmetic coding, and Quad-tree coding. Metrics like the MSE, PSNR, and Entropy were employed as well."
      context_short: Coding of 2D image compression
      achievements:
        - "\\textbf{C++ language} used in the development of this project"
        - "\\textbf{Image compression:} DCT transforms, Quantization tables, Runlength coding, Delta coding, Arithmetic coding, and Quad-tree coding."
      achievements_short:
        - "C++"
        - "Image compression"
    - name: Communication Channels
      start: February 2018
      end: May 2018
      company: VUB
      location: Brussels
      context: "In the context of the communication channels course there was a project about the simulation of EM waves.\\\\A simulation program was developed in MATLAB that employs the Finite Difference Time Domain method to simulate how EM waves traverse the 2D space and interact with each other.\\\\Since I realized the importance of version control software, I tried my hand with git for the first time in the context of this project."
      context_short: Development of EM simulator
      achievements:
        - "\\textbf{Matlab:} used as tool in the context of this project"
        - "\\textbf{Git:} also employed as version control for this project"
        - "Finite Difference Time Domain method employed for the simulator"
      achievements_short:
        - "Matlab"
        - "Git"
        - "FDTD method"
    - name: Image Processing
      start: February 2018
      end: May 2018
      company: VUB
      location: Brussels
      context: "In the context of the image processing course there was a project as well. Here the project varied a lot from team to team. My team and I chose to develop a program that creates Mosaics from an input photo and a given database.\\\\The developed program was based on objective quality assessment and its use in comparing two pictures to select the best match in the database for a subpicture of the input. A series of IQAs (Image Quality Assessments) have been employed of which the two most common were the PSNR and SSIM."
      context_short: Development of a Mosaic generation program
      achievements:
        - "\\textbf{Matlab:} used as tool in the context of this project"
        - "\\textbf{Image processing:} use of IQAs"
      achievements_short:
        - "Matlab"
        - "Image Processing"
    - name: Image Processing
      start: February 2018
      end: May 2018
      company: VUB
      location: Brussels
      context: "A project to develop a basic modulation scheme (DVB-S2 Communication chain). A Matlab implementation of four different constellations (BSPK, APSK, 16QAM, 64QAM) has been designed in the context of this project, a Nyquist filter is used to limit the communication bandwidth and negate the inter-symbol interferences, a LDPC (low density parity check) coder is used to achieve a measure of noise resilience, and a scheme for frequency and time synchronization was coded as well"
      context_short: Development of communication chain
      achievements:
        - "\\textbf{Algorithm:} constellations, Nyquist filter, LDPC, and frequency and time synchronization"
        - "\\textbf{Result:} successfully implemented a DVB-S2 communication chain"
        - "\\textbf{Technologies:} Matlab"
        - "\\textbf{Skills:} coding of communication chain in Matlab, Teamwork"
      achievements_short:
        - "Communication chain"
        - "Matlab"
    - name: Sensors and ucontrollers
      start: February 2018
      end: May 2018
      company: VUB
      location: Brussels
      context: "A project was organized to train our skills with u-controllers. For this project, you were allowed to choose some program/game to develop for the Genuino UNO (Atmel AT328P) in the corresponding assembly language. The board with the controller held a LED-display, a series of buttons, and a buzzer."
      context_short: Development of game on Atmel AT328P
      achievements:
        - "\\textbf{Result:} successfully implemented Tetris on the given ucontroller"
        - "\\textbf{Technologies:} Assembly AVR Architecture"
        - "\\textbf{Skills:} Embedded programming, Assembly, Teamwork"
      achievements_short:
        - "Assembly AVR Architecture"
        - "Embedded programming"
  skills:
    - name: C programming
      score: 3
    - name: Git
      score: 3
    - name: Shell
      score: 3
    - name: Python3 programming
      score: 3
    - name: C++ programming
      score: 2
    - name: Embedded Linux
      score: 2
    - name: Docker
      score: 2
    - name: Linux
      score: 2
    - name: Cross Compiling
      score: 2
    - name: Linux Device Drivers
      score: 1
    - name: Buildroot
      score: 1
    - name: CAN
      score: 1
    - name: x86 Assembly
      score: 1
    - name: VHDL
      score: 1
    - name: Latex
      score: 1
  studies: "Studied Electrical Engineering at the VUB, option Information and Telecommunication Technology Systems, 2019"
